Files used: NGC4993_lowerDIS.dat, tempel2016_cata_full.out

The position used for the source is: RA = 197.45 (deg), DEC = -23.38 (deg), DIS = 26.00 (Mpc)
The number of sources in the catalog is: 12106
The number of sources (without zero masses) in the catalog is: 5166


The Shapiro delay from 5166 sources is 2.80e+04 days = 77 years


