There are 3 codes that generate the various results and figures of the manuscript LIGO-P1900149:

    - Shapiro_main_code_TABLE1_FIG2.ipynb : generates the results in table 1, as well as the figure 2 (conversion in *.py format available). It is the main notebook in the sense that it details the main calculation with LaTeX parts.
    - Shapiro_FIG1.ipynb : generates the figure 1 (conversion in *.py format available). This code can take a few hours to run. One can decrease the resolution of the skymap in order to reduce the computing time, by setting "interval = 10." in the third block.
    - Shapiro_FIG3.ipynb : generates the figure 3 (conversion in *.py format available).

The results are saved in the various folders in order to be recovered after the runs if needed.
In order to get the results and plots for the various catalogs in "Shapiro_main_code_TABLE1_FIG2.ipynb" and "Shapiro_FIG3.ipynb", one needs to edit the file "file_names.in" with the name of the desired catalog: either "tempel2016_cata_full", or "tully2015_cata_Mlum_icrs", or "tully2015_cata_Mvir_icrs", and then rerun the codes.

In the folder "catalogs_data", there are two codes that translate the original catalogs' data into a format readable by the previously mentioned codes. The result of the translations are saved in this folder, to be called by the 3 codes mentioned above. The catalogs themselves are public and can be found at the following addresses:

    - http://cdsarc.u-strasbg.fr/viz-bin/cat/J/AJ/149/171 
    - http://cdsarc.u-strasbg.fr/viz-bin/cat/J/A+A/588/A14

The corresponding papers are:

    - https://doi.org/10.1088/0004-6256/149/5/171
    - https://doi.org/10.1051/0004-6361/201527755

Consent has been obtained from the authors to reproduce a subset of their data at the following location:
    
    - /catalogs_data/tempel2016_original_data
    - /catalogs_data/tully2015_original_data 


The present project is licensed under GNU General Public License v3.0. 
--------------------------------------------------------------------------------------------



Below, email exchange with AAS regarding copyright issues:
--------------------------------------------------------------------------------------------
 Dear Olivier Minazzoli, 

You may reuse supplementary data. Raw data are not subject to copyright. Therefore, you can reuse the data in other publications or pieces of work, provided that you cite its source. However, pictorial or graphical representations of the data, such as figures or graphs, are protected by copyright. Therefore, you would need to request permission from IOP (or the copyright owner, if not IOP) to reuse the particular figure/graph.

Kind regards, 
Christina

Copyright & Permissions Team
Gemma Alaway – Senior Rights & Permissions Adviser
Christina Colwell - Rights & Permissions Assistant

Contact Details
E-mail: permissions@iop.org

For further information about copyright and how to request permission: https://publishingsupport.iopscience.iop.org/copyright-journals/

See also:  https://publishingsupport.iopscience.iop.org/

Please see our Author Rights Policy https://publishingsupport.iopscience.iop.org/author-rights-policies/

Please note: We do not provide signed permission forms as a separate attachment. Please print this email and provide it to your publisher as proof of permission.

Please note: Any statements made by IOP Publishing to the effect that authors do not need to get permission to use any content where IOP Publishing is not the publisher is not intended to constitute any sort of legal advice. Authors must make their own decisions as to the suitability of the content they are using and whether they require permission for it to be published within their article.

 


From: Olivier Minazzoli <olivier.minazzoli@ligo.org>
Sent: 25 July 2019 10:59
To: permissions@iop.org <permissions@iop.org>
Cc: Nathan Johnson-McDaniel <nkj29@cam.ac.uk>; Sakellariadou, Mairi <mairi.sakellariadou@kcl.ac.uk>
Subject: Permission from Brent Tully to reuse catalog data
 
To whom it may concern,

I'm following the procedure written in the link below to make sure that there is not any copyright issue with a gitlab repository and a paper that we are planning to submit soon with my co-authors (in CC).

https://journals.aas.org/article-charges-and-copyright/#reproduction_of_copyrighted_materials

In our paper, we use a subset of the catalog from Brent Tully "2MASS galaxy group catalog" -- found at http://cdsarc.u-strasbg.fr/viz-bin/cat/J/AJ/149/171. The corresponding paper is

    The Astronomical Journal, Volume 149, Issue 5, article id. 171, 14 pp. (2015).

    Additionally, we plan to put our codes in a gitlab repository hosted by the LIGO collaboration. But we wish to put the data that we used in the repository as well, such that people can simply run the codes after downloading the repository, without having to download the public data from themselves.

    Brent Tully granted the permission to do so to us.

    We now would like to have a formal permission, as mentioned in the third point of the link above, which we would eventually leave on the LIGO gitlab repository.

    Many thanks for your help.

    Best regards,

    Olivier Minazzoli on the behalf of the authors.




IOP Publishing email addresses have changed from @iop.org to @ioppublishing.org, except those of our legal and finance teams, which have changed to @ioplegal.org and @iopfinance.org respectively.

This email (and attachments) are confidential and intended for the addressee(s) only. If you are not the intended recipient please immediately notify the sender, permanently and securely delete any copies and do not take action with it or in reliance on it. Any views expressed are the author's and do not represent those of IOPP, except where specifically stated. IOPP takes reasonable precautions to protect against viruses but accepts no responsibility for loss or damage arising from virus infection. For the protection of IOPP's systems and staff; emails are scanned automatically.

IOP Publishing Limited
Registered in England under Registration No 00467514.
Registered Office: Temple Circus, Bristol BS1 6HG England

Your privacy is important to us. For information about how IOPP uses your personal data, please see our Privacy Policy

--------------------------------------------------------------------------------------------
