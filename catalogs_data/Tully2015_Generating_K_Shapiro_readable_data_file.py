#!/usr/bin/env python
# coding: utf-8

# # Code that translates datafile from Tully2015 to datafile that can be read in the code K_Shapiro

# Author: Olivier Minazzoli

# date (yymmdd): 180112

# Code in Python 3 that reads the catalogs given in [Tully 2015] and write it in the format that is used in the code K_Shapiro [Minazzoli 2019]

# ## References

# - [Tully 2015]: http://cdsarc.u-strasbg.fr/viz-bin/cat/J/AJ/149/171
# - [Minazzoli 2019]: https://git.ligo.org/olivier.minazzoli/shapiro_cosmo_nogo_paper/blob/master/code/K_Shapiro.ipynb

# In[1]:


# Import scientific package numpy
import numpy as np
# Import package for data manipulation pandas
import pandas as pd


# In[2]:


# Import various tools to deal with coordinate systems in astronomy
from astropy.coordinates import SkyCoord  # High-level coordinates
from astropy.coordinates import ICRS, Galactic, FK4, FK5  # Low-level frames
from astropy.coordinates import Angle, Latitude, Longitude  # Angles
import astropy.units as u


# In[3]:


# Import data into pandas dataframe
pd_arrays = pd.read_fwf('tully2015_original_data/tully2015AJ_table5.dat', header= None)


# ## Create numpy arrays vectors (with as.matrix()) from the various rows of the dataframe

# In[4]:


cata_long = pd_arrays[1].as_matrix()           # longitude given in galactic frame (deg)
cata_lat = pd_arrays[2].as_matrix()            # latitude given in galactic frame (deg)
cata_dist = pd_arrays[18].as_matrix()          # (Mpc)
cata_mass_vir = pd_arrays[33].as_matrix()*1e12 # (M_SUN)
cata_mass_lum = pd_arrays[34].as_matrix()*1e12 # (M_SUN)
cata_nest = pd_arrays[15].as_matrix()          # number


# In[5]:


tully2015_cata_Mlum_galactic = np.c_[cata_long,cata_lat,cata_dist,cata_mass_lum]
tully2015_cata_Mvir_galactic = np.c_[cata_long,cata_lat,cata_dist,cata_mass_vir]


# ## Convert into ICRS

# In[6]:


# Create SkyCoord vectors from data in galactic coordinates
#vec_coord_galactic = SkyCoord(cata_long[:]*u.deg, cata_lat[:]*u.deg, distance = cata_dist[:]*u.mpc, frame='galactic')
vec_coord_galactic = SkyCoord(cata_long[:], cata_lat[:], distance = cata_dist[:], unit = (u.degree,u.degree,u.mpc), frame='galactic')

vec_coord_icrs = vec_coord_galactic.icrs
#vec_coord_icrs = vec_coord_galactic.transform_to('icrs')

# Convert vectors from galactic to icrs coordinates
cata_ra = vec_coord_icrs.ra.degree
cata_dec = vec_coord_icrs.dec.degree


# ## Concatenate the positions vectors with either the mass luminosity or the mass from the virial theorem (not all objects have a mass from the virial theorem)

# In[7]:


tully2015_cata_Mlum_icrs = np.c_[cata_ra,cata_dec,cata_dist,cata_mass_lum]
tully2015_cata_Mvir_icrs = np.c_[cata_ra,cata_dec,cata_dist,cata_mass_vir]


# ## Search for doublons from nest ID

# In[8]:


# Set list with Nest ID already used
nest_already_vir = []
nest_already_lum = []


# In[9]:


Nbr_unnested_sources_vir = len(tully2015_cata_Mvir_icrs)
Nbr_unnested_sources_lum = len(tully2015_cata_Mlum_icrs)
print("# sources : {:.0f},{:.0f} (vir,num)".format(Nbr_unnested_sources_vir,Nbr_unnested_sources_lum))


# In[10]:


# Set array
tully2015_cata_Mvir_icrs_sorted = np.empty([0,4])
tully2015_cata_Mlum_icrs_sorted = np.empty([0,4])

# Write only non-doublons
for i in range(Nbr_unnested_sources_vir):
    if cata_nest[i] not in nest_already_vir:
        nest_already_vir.append(cata_nest[i])
        tully2015_cata_Mvir_icrs_sorted = np.insert(tully2015_cata_Mvir_icrs_sorted, 0 ,tully2015_cata_Mvir_icrs[i],axis = 0 )
        
for i in range(Nbr_unnested_sources_lum):
    if cata_nest[i] not in nest_already_lum:
        nest_already_lum.append(cata_nest[i])
        tully2015_cata_Mlum_icrs_sorted = np.insert(tully2015_cata_Mlum_icrs_sorted, 0 ,tully2015_cata_Mlum_icrs[i],axis = 0 )


# In[11]:


print("# remaining sources :{:.0f},{:.0f} (vir,lum)".format(len(tully2015_cata_Mvir_icrs_sorted),len(tully2015_cata_Mlum_icrs_sorted)))


# In[12]:


# Erase all the rows with nan from missing mass from the virial theorem
tully2015_cata_Mvir_icrs_sorted = tully2015_cata_Mvir_icrs_sorted[~np.isnan(tully2015_cata_Mvir_icrs_sorted).any(axis=1)]

tully2015_cata_Mlum_icrs_sorted = tully2015_cata_Mlum_icrs_sorted[~np.isnan(tully2015_cata_Mlum_icrs_sorted).any(axis=1)]


# In[13]:


print("# remaining sources :{:.0f},{:.0f} (vir,lum)".format(len(tully2015_cata_Mvir_icrs_sorted),len(tully2015_cata_Mlum_icrs_sorted)))


# In[14]:


# Save data to files
np.savetxt('tully2015_cata_Mlum_icrs.out',tully2015_cata_Mlum_icrs_sorted, fmt = '%f')
np.savetxt('tully2015_cata_Mvir_icrs.out',tully2015_cata_Mvir_icrs_sorted, fmt = '%f')

