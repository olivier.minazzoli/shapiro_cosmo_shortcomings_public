#!/usr/bin/env python
# coding: utf-8

# # Code that translates datafile from Tempel2016 to datafile that can be read in the code K_Shapiro

# Author: Olivier Minazzoli

# date (yymmdd): 180112

# Code in Python 2 that reads the catalogs given in [Tempel et al 2016] and write it in the format that is used in the code K_Shapiro [Minazzoli 2019]

# ## References

# - [Tempel et al. 2016]: http://cdsarc.u-strasbg.fr/viz-bin/Cat?J/A%2bA/588/A14
# - [Minazzoli 2019]: https://git.ligo.org/olivier.minazzoli/shapiro_cosmo_nogo_paper/blob/master/code/K_Shapiro.ipynb

# In[1]:


# Import scientific package numpy
import numpy as np
# Import package for data manipulation pandas
import pandas as pd


# In[2]:


# Import various tools to deal with coordinate systems in astronomy
from astropy.coordinates import SkyCoord  # High-level coordinates
from astropy.coordinates import ICRS, Galactic, FK4, FK5  # Low-level frames
from astropy.coordinates import Angle, Latitude, Longitude  # Angles
import astropy.units as u


# In[3]:


# Import data into pandas dataframe
pd_arrays = pd.read_fwf('tempel2016_original_data/tempel2016_table3.dat', header= None)      # Catalog (6282#) of 2mrs groups (http://cdsarc.u-strasbg.fr/viz-bin/Cat?J/A%2bA/588/A14)
pd_arrays_full = pd.read_fwf('tempel2016_original_data/tempel2016_table4.dat', header= None) # Catalog (12106#) of combined dataset groups (http://cdsarc.u-strasbg.fr/viz-bin/Cat?J/A%2bA/588/A14)


# ## Create numpy arrays vectors (with as.matrix()) from the various rows of the dataframe

# In[4]:


cata_ra = pd_arrays[2].as_matrix()           # right ascension (J2000) frame (deg)
cata_dec = pd_arrays[3].as_matrix()          # declination (J2000) frame (deg)
cata_dis = pd_arrays[9].as_matrix()          # distance (Mpc)
cata_mass = pd_arrays[13].as_matrix()*1e12   # estimated mass of NFW potentials (MSUN)

cata_ra_full = pd_arrays_full[2].as_matrix()           # right ascension (J2000) frame (deg)
cata_dec_full = pd_arrays_full[3].as_matrix()          # declination (J2000) frame (deg)
cata_dis_full = pd_arrays_full[9].as_matrix()          # distance (Mpc)
cata_mass_full = pd_arrays_full[13].as_matrix()*1e12   # estimated mass of NFW potentials (MSUN)


# ## Concatenate vectors to get data matrix and save it to a file

# In[5]:


tempel2016_cata = np.c_[cata_ra,cata_dec,cata_dis,cata_mass]

tempel2016_cata_full = np.c_[cata_ra_full,cata_dec_full,cata_dis_full,cata_mass_full]


# In[6]:


np.savetxt('tempel2016_cata.out',tempel2016_cata, fmt = '%e')

np.savetxt('tempel2016_cata_full.out',tempel2016_cata_full, fmt = '%e')


# ## Rewrite file without rows with null mass

# In[7]:


Nbr_sources_cata = len(tempel2016_cata)
Nbr_sources_cata_full = len(tempel2016_cata_full)


# In[8]:


tempel2016_cata_wo0 = np.empty([0,4])
tempel2016_cata_full_wo0 = np.empty([0,4])

for i in range(Nbr_sources_cata):
    if cata_mass[i] != 0:
       tempel2016_cata_wo0 = np.insert(tempel2016_cata_wo0,0,tempel2016_cata[i], axis = 0)

for i in range(Nbr_sources_cata_full):
    if cata_mass_full[i] != 0:
       tempel2016_cata_full_wo0 = np.insert(tempel2016_cata_full_wo0,0,tempel2016_cata_full[i], axis = 0)


# In[9]:


np.savetxt('tempel2016_cata_wo0.out',tempel2016_cata_wo0, fmt = '%e')

np.savetxt('tempel2016_cata_full_wo0.out',tempel2016_cata_full_wo0, fmt = '%e')

